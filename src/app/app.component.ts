import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

import { firstValueFrom } from 'rxjs'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  data? : any

  constructor(private appService : AppService){}

  ngOnInit(): void {
    this.getData()    
  }

  async getData() : Promise<void> {
    const result = await firstValueFrom(this.appService.getData())
    this.data = result.data
  }
}
